const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const Mongo = require("./db/mongo");
const port = parseInt(process.env.PORT, 10) || 6001;

const app = express();

app.use(helmet()); // enhance API security
app.use(bodyParser.urlencoded({extended: true})); // parse JSON into objects
app.use(cors()); // enable CORS for all req
app.use(morgan("combined")); // logger

/** Inject router **/
const router = express.Router();
require("./route/routes")(router);
app.use(router);

if (process.env.NODE_ENV !== "test") { // avoid unit test conflicts on port
    Mongo.startDB().then(async () => {
        app.listen(port, err => {  // start the server
            if (err) throw err;
            console.log(`>> Ready on http://localhost:${port}`);
        });
    });
}

module.exports = app;
