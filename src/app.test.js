const request = require("supertest");
const {MongoClient} = require('mongodb');
const app = require("./app");

/**
 * Get all properties
 */
describe("app", () => {
    describe("GET /property : Success Path", () => {
        it("respond with json containing a list of all properties", done => {
            request(app)
                .get("/property")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200, done);
        });
    });

    describe("POST /property CREATE", () => {
        it("save a new property", done => {
            request(app)
                .post('/property')
                .query({name: 'Property 1'})
                .query({price: '31'})
                .expect(201)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });

        it("should return a 400: Bad Response when query params are missing", (done) => {
            request(app)
                .post('/property')
                .expect(400)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe("PATCH /property/{id} UPDATE", () => {
        it("update a property", done => {

            request(app)
                .post('/property')
                .query({name: 'Property 1'})
                .query({price: '31'})
                .end(function (err, res) {
                    if (err) return done(err);
                    const id = res.body;

                    request(app)
                        .patch(`/property/${id}`)
                        .query({name: 'Property Changed'})
                        .query({price: '99'})
                        .expect(204)
                        .end(function (err, res) {
                            if (err) return done(err);
                            done();
                        });
                });
        });

        it("should return a 400: Bad Response when query params are missing", (done) => {
            request(app)
                .patch('/property/1')
                .expect(400)
                .end(function (err, res) {
                    if (err) return done(err);
                    done();
                });
        });
    });

    describe("DELETE /property/{id} UPDATE", () => {
        it("delete a property", done => {
            request(app)
                .post('/property')
                .query({name: 'Property 1'})
                .query({price: '31'})
                .end((err, res) => {
                    if (err) return done(err);
                    const id = res.body;

                    request(app)
                        .delete(`/property/${id}`)
                        .expect(200)
                        .end(err => {
                            if (err) return done(err);
                            done();
                        });
                })
        })
    });
});
