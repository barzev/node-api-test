const {getDefaultCollection, createProperty, updateProperty, removeProperty} = require("../db/property");

const getAll = async (req, res) => {
    res.status(200).send(await getDefaultCollection());
};

const create = async (req, res) => {

    const {name, price} = req.query;

    res.status(201).send(await createProperty({name, price}));
};

const update = async (req, res) => {
    const {name, price} = req.query;

    res.status(204).send(await updateProperty(req.params.id, {name, price}));
};

const remove = async (req, res) => {
    res.status(200).send(await removeProperty(req.params.id))
};


module.exports = {getAll, create, update, remove};