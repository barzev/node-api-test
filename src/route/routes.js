const { getAll, create, update, remove } = require("../service/services");
const { validateQuery } = require("./middleware");

module.exports = function (router) {
    router.get("/property", getAll);
    router.post("/property", [validateQuery(["name", "price"])], create);
    router.patch("/property/:id", [validateQuery(["name", "price"])], update);
    router.delete("/property/:id", remove);
};

