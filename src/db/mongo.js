const {MongoClient} = require("mongodb");
const {MongoMemoryServer} = require('mongodb-memory-server');
const path = require("path");

let database = null;

async function startDB() {
    const connection = await MongoClient
        .connect(await determineDBUri(), {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        });
    database = connection.db();
}

async function getDB() {
    if (!database) await startDB();
    return database;
}

/** Helper fns **/
const determineDBUri = async () =>
    process.env.NODE_ENV === "test"
        ? await new MongoMemoryServer().getConnectionString()
        : process.env.MONGOdb_URI || require(path.resolve(__dirname, "./config.js")).uri;


module.exports = {startDB, getDB};