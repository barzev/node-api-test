const {ObjectId} = require('mongodb'); // or ObjectID

const {getDB} = require("./mongo");
const path = require('path');
const defaultCollection = require(path.resolve(__dirname, "./config.js")).defaultCollection;

async function createProperty(property) {
    const database = await getDB();

    const {insertedId} = await database
        .collection(defaultCollection)
        .insertOne(property);
    return insertedId;
}

async function getDefaultCollection() {
    const database = await getDB();

    return database
        .collection(defaultCollection)
        .find({})
        .toArray();
}

async function updateProperty(id, property) {
    const database = await getDB();

    return database
        .collection(defaultCollection)
        .updateOne(
            {"_id": ObjectId(id)}, // Filter
            {$set: {...property}}, // Update
        );
}

async function removeProperty(id) {
    const database = await getDB();

    return database
        .collection(defaultCollection)
        .deleteOne({"_id": ObjectId(id)});
}

module.exports = {createProperty, getDefaultCollection, updateProperty, removeProperty};
