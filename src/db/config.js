// Note - password & URI would be best loaded from an environment variable, but leaving it here for simplicity
const uri = "mongodb+srv://admin:scrayetestapi123@cluster0-ie9sh.mongodb.net/test?retryWrites=true&w=majority";
const defaultCollection = "properties";

module.exports = {uri, defaultCollection};
