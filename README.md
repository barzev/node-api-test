# Scraye Node API Test #

### Solution: API built as per requirements sheet.

### Technologies: Node.js, Express, MongoDB (+ MongoDB Atlas), Jest, Supertest, In-memory Mongo for tests 
 
## Details:
The API is set up to run on Port 6001.

How to run server:

0. Prerequisites : Node, npm
1. npm run install
2. npm start

How to run tests:

0. Prerequisites : Node, npm
1. npm run install (if not ran before)
2. npm run test
3. npm run test:watch (Watch Mode)
4. Tests run with in-memory MongoDB so not affecting liveDB

### Here's a view of the MongoDB Cluster and the properties collection
![MongoDB Atlas properties collection screenshot example](mongoDBAtlas.png)

Using an actual live DB hosted on AWS with MongoDB.Atlas cloud

## Extra notes:
Took about 3-4 hours to build - with a few breaks and interruptions in between.

Next steps would be: 
- more testing - increase coverage, focus on adding more unit tests
- add authentication & endpoint protection (e.g. auth0)
- improve validation - edge cases, existing objects handling, 
- improve error handling 
- better user messages
- create live API docs